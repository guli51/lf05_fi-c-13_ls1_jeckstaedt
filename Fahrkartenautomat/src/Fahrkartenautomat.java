﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    { 
       double zuZahlenderBetrag; 
       double rückgabebetrag;


       zuZahlenderBetrag = fahkartenbestellungErfassen();
       
       // Geldeinwurf
       // -----------
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(rückgabebetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
 
    }
    
    public static double fahkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double ticketpreis;
    	int anzahlTickets;

    	System.out.print("Ticketpreis (EURO): ");
        ticketpreis = tastatur.nextDouble();

        System.out.print("Anzahl der Tickets: ");
        anzahlTickets = tastatur.nextInt();
        if(anzahlTickets < 1 || anzahlTickets > 10) {
        	
        	anzahlTickets = 1;
        	System.out.println("Ungültige Ticketanzahl! Ticketanzahl wurde auf 1 gesetzt!");
        }
        
        //tastatur.close();
        return ticketpreis * anzahlTickets;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMünze;
    	
    	while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	//tastatur.close();
    	
    	return eingezahlterGesamtbetrag - zuZahlen;

    }
    
    public static void fahrkartenAusgeben() {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
        	System.out.print("=");
        	warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rueckgeld) {
    	
    	if(rueckgeld > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rueckgeld);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgeld >= 2.0) // 2 EURO-Münzen
            {
         	 muenzeAusgeben(2, " EURO");
         	 rueckgeld -= 2.0;
            }
            while(rueckgeld >= 1.0) // 1 EURO-Münzen
            {
            	muenzeAusgeben(1, " EURO");
         	 rueckgeld -= 1.0;
            }
            while(rueckgeld >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50, " CENT");
         	 rueckgeld -= 0.5;
            }
            while(rueckgeld >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20, " CENT");
         	 rueckgeld -= 0.2;
            }
            while(rueckgeld >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10, " CENT");
         	 rueckgeld -= 0.1;
            }
            while(rueckgeld >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5, " CENT");
         	 rueckgeld -= 0.05;
            }
        }
    }
    public static void warte(int millisekunde) {
    	
    	try {
        	Thread.sleep(millisekunde);
           	} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
           	}
    }
    
   public static void muenzeAusgeben(int betrag, String einheit) {
	   
	   System.out.println(betrag + einheit);
	   
   }
}

// Aufgabe 5: Ich habe den Datentyp int für die Anzahl der Tickets gewählt, da nur ganze Tickets gedruckt werden können. Byte, Short und long wären ebenfalls möglich.
// Aufgabe 6: Bei der Rechnung ticketpreis * anzahlTickets wird ein Integerwert mit einem Doublewert multipliziert. Als Ergebnis aus dieser Rechnung kommt ein Doublewert raus. Bsp.: 2,0 * 2 = 4,0