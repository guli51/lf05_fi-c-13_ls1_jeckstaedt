public class Temperaturtabelle{
	
	public static void main(String[] args) {
		
		double a = -28.8889;
		double b = -23.3333;
		double c = -17.7778;
		double d = -6.6667;
		double e = -1.1111;
		int f = 20;
		int g = 10;
		int h = 0;
		
		/*System.out.println("Fahrenheit |   Celsius");
		System.out.println("----------------------");
		System.out.printf("-20        |    %.2f\n", a);
		System.out.printf("-10        |    %.2f\n", b);
		System.out.printf("+0         |    %.2f\n", c);
		System.out.printf("+20        |    %.2f\n", d);
		System.out.printf("+30        |    %.2f\n", e);*/
		
		System.out.println("Fahrenheit  |  Celsius");
		System.out.println("----------------------");
		System.out.printf("%-12d|%9.2f\n", -f, a);
		System.out.printf("%-12d|%9.2f\n", -g, b);
		System.out.printf("+%-11d|%9.2f\n", h, c);
		System.out.printf("+%-11d|%9.2f\n", f, d);
		System.out.printf("+%-11d|%9.2f\n", 30, e);
	}
}