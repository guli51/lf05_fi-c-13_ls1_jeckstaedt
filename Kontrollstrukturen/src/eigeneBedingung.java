import  java.util.Scanner;

public class eigeneBedingung {

	public static void main(String args[]) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int zahl1, zahl2, zahl3;
		
		System.out.println("Geben Sie drei Zahlen ein: ");
		
		zahl1 = tastatur.nextInt();
		zahl2 = tastatur.nextInt();
		zahl3 = tastatur.nextInt();
		
		if(zahl1 == zahl2) {
			
			System.out.println("Zahlen stimmen überein");
		}
		
		if(zahl2 > zahl1) {
			
			System.out.println("Zahl 2 ist größer als Zahl 1");
		}
		
		if(zahl1 >= zahl2) {
			
			System.out.println("Zahl 1 ist größer gleich Zahl 2");
		}
		else {
			
		System.out.println("Zahl 2 ist größer als Zahl 1");
		}
		
		if(zahl1 == zahl2 && zahl1 == zahl3) {
			
			System.out.println("Zahl 1, 2 und 3 sind gleich");
		}
		
		if(zahl3 > zahl2 || zahl3 > zahl1) {
			
			System.out.println("Zahl 3 ist größer als Zahl 1 oder Zahl 2");
			
		}
		
		if(zahl3 > zahl2 && zahl3 > zahl1) {
			
			System.out.println("Zahl 3 ist die Größte");	
		}
		
		else if(zahl2 > zahl1 && zahl2 > zahl3) {
			
			System.out.println("Zahl 2 ist die Größte");
		}
		
		else if(zahl1 > zahl3 && zahl1 > zahl2) {
			
			System.out.println("Zahl 1 ist die Größte");
		}
		
	}
}
