package eingabeAusgabe;

import java.util.Scanner;

public class Begruessung {
	public static void main(String[] args) // Hier startet das Programm
	 {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Hallo Benutzer. Bitte geben Sie Ihren Namen an: ");
		String name = myScanner.next();
		
		System.out.print("Bitte geben Sie noch Ihr Alter an: ");
		int alter = myScanner.nextInt();
		
		System.out.println("Sie heißen " + name + " und sind " + alter + " Jahre alt.");
		myScanner.close();
	 }
}
