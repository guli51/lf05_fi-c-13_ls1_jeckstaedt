import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int seite;
		
		System.out.println("Bitte geben Sie die Seitenlänge an: ");
		seite = tastatur.nextInt() - 1;
		
		for(int i = 0; i <= seite; i++) {
			for(int j = 0; j <= seite; j++) {
				
				if(i == 0 || i == seite || j == 0 || j == seite) {
					System.out.print(" * ");
				}
				else {
					System.out.print("   ");
				}
			}
			System.out.println("");
		}
	}
}
