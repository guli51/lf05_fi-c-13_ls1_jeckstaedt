import java.util.Scanner;

public class Ohmsche_Gesetz {
	
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		char formel;
		double zahl1, zahl2;
		
		System.out.println("Geben Sie bitte das Formelzeichen an, welches Sie berechnen möchten(R, I oder U): ");
		formel = tastatur.next().charAt(0);
		
		while(formel != 'R' && formel != 'U' && formel != 'I') {
			
			System.out.println("Falsche Eingabe. Bitte erneut eingeben: ");
			formel = tastatur.next().charAt(0);
		}
		
		switch(formel) {
		
		case 'R':
			System.out.println("Bitte geben U an: ");
			zahl1 = tastatur.nextDouble();
			
			System.out.println("Bitte geben I an: ");
			zahl2 = tastatur.nextDouble();
			
			System.out.printf("R beträgt %.2f Ohm.", zahl1 / zahl2);
			break;
			
		case 'U':
			System.out.println("Bitte geben R an: ");
			zahl1 = tastatur.nextDouble();
			
			System.out.println("Bitte geben I an: ");
			zahl2 = tastatur.nextDouble();
			
			System.out.printf("U beträgt %.2f V.", zahl1 * zahl2);
			break;
			
		case 'I':
			System.out.println("Bitte geben U an: ");
			zahl1 = tastatur.nextDouble();
			
			System.out.println("Bitte geben R an: ");
			zahl2 = tastatur.nextDouble();
			
			System.out.printf("I beträgt %.2f A.", zahl1 / zahl2);
			break;
			
		}
	}

}
