import java.util.Scanner;

public class BMI {
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int gewicht;
		double bmi, groesse;
		char geschlecht;
		
		System.out.print("Bitte geben Sie Ihr Gewicht gerundet in Kilogram an: ");
		gewicht = tastatur.nextInt();
		
		System.out.print("Bitte geben Sie Ihre Größe gerundet in Centimieter an: ");
		groesse = tastatur.nextDouble();
		groesse = groesse/100;
		
		
		System.out.print("Sind Sie männlich(m) oder weiblich(w)?");
		geschlecht = tastatur.next().charAt(0);
		
		if(geschlecht != 'm' && geschlecht != 'w') {
			System.out.println("Eingabe ungültig!");
			System.out.print("Sind Sie männlich(m) oder weiblich(w)?");
			geschlecht = tastatur.next().charAt(0);
		}
		
		bmi = gewicht / (groesse*groesse);
		
		System.out.printf("BMI:[%.1f]\n", bmi);
		
		klassifizierung(bmi, geschlecht);
		
	}
	
	public static void klassifizierung(double bmi, char geschlecht) {
		
		if(geschlecht == 'm') {
			
			if(bmi < 20) {
				System.out.println("Sie sind untergewichtig.");
			}
			else if(bmi >= 20 && bmi <= 25) {
				System.out.println("Sie sind normalgewichtig.");
			}
			else if(bmi > 25) {
				System.out.println("Sie sind übergewichtig.");
			}
		}
		
		else if(geschlecht == 'w') {
			
			if(bmi < 19) {
				System.out.println("Sie sind untergewichtig.");
			}
			else if(bmi >= 19 && bmi <= 24) {
				System.out.println("Sie sind normalgewichtig.");
			}
			else if(bmi > 24) {
				System.out.println("Sie sind übergewichtig.");
			}
		}
	}

}
