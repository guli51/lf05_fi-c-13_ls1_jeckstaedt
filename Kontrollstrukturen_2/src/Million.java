import java.util.Scanner;

public class Million {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		double einlage, zinssatz, stand;
		int count;
		char weiter;
		
		do {
			count = 0;
			System.out.println("Bitte geben Sie Ihre Einlage an: ");
			einlage = tastatur.nextDouble();
		
			System.out.println("Bitte geben Sie den Zinssatz an: ");
			zinssatz = tastatur.nextDouble();
		
			stand = einlage;
		
			while(stand < 1000000) {
				stand = stand + (stand * zinssatz)/100;
				count++;
			}
		
			System.out.printf("Es dauerte %d Jahre bis Sie Millionär geworden sind.\n", count);
			System.out.println("Möchten Sie noch eine Rechnung durchführen? (j/n) ");
			weiter = tastatur.next().charAt(0);
		}
		while(weiter == 'j');
	}
}
