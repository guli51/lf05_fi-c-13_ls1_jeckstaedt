import java.util.Scanner;

public class Matrix {
	
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int eingabe;
		
		System.out.println("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		eingabe = tastatur.nextInt();
		
		for(int i = 0; i < 100; i += 10) {
			for(int j = 0; j < 10; j++) {
				//Test ob durch 4 teilbar und != 0	|	Test ob die Eingabe enthalten ist in Zahl |	Testet die Quersumme
				if(((i+j) % eingabe == 0 && i+j != 0) || j == eingabe || (i/10) == eingabe || (i/10)+j == eingabe) {
					System.out.print("   *");
				}
				else {
					System.out.printf("%4d", i+j);
				}
			}
			System.out.println("");
		}
	}

}
