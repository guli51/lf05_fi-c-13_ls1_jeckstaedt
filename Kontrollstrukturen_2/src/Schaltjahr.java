import java.util.Scanner;

public class Schaltjahr {
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
	
		int jahr;
	
		System.out.println("Bitte geben Sie ein Jahr ein:");
		jahr = tastatur.nextInt();
	
		if(jahr % 4 == 0 && jahr % 100 != 0) {
			
			System.out.printf("Das Jahr %d ist ein Schaltjahr.", jahr);
		
		}
		
		else if(jahr % 400 == 0) {
			System.out.printf("Das Jahr %d ist ein Schaltjahr.", jahr);
		}
		else {
			System.out.printf("Das Jahr %d ist kein Schaltjahr.", jahr);
		}
	
	
	}
}
