import java.util.Scanner;

public class Taschenrechner {
	
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int  zahl2;
		char zeichen;
		double zahl1, ergebnis;
		
		System.out.print("Bitte geben Sie die erste Zahl an: ");
		zahl1 = tastatur.nextDouble();
		
		System.out.print("Bitte geben Sie die zweite Zahl an: ");
		zahl2 = tastatur.nextInt();
		
		System.out.print("Bitte geben Sie die Rechenoperation an: ");
		zeichen = tastatur.next().charAt(0);
		if(zeichen != '+' && zeichen != '-' && zeichen != '*' && zeichen != '/' ) {
			System.out.println("Falsche Eingabe");
			return;
		}
		
		switch(zeichen) {
		
		case '+':
			ergebnis = zahl1 + zahl2;
			System.out.printf("Das Ergebnis ist %.2f", ergebnis);
			break;
		case '-':
			ergebnis = zahl1 - zahl2;
			System.out.printf("Das Ergebnis ist %.2f", ergebnis);
			break;
		case '*':
			ergebnis = zahl1 * zahl2;
			System.out.printf("Das Ergebnis ist %.2f", ergebnis);
			break;
		case '/':
			ergebnis = zahl1 / zahl2;
			System.out.printf("Das Ergebnis ist %.2f", ergebnis);
			break;
		}
		
	}

}
